import csv
import math
import re

from sklearn.metrics import classification_report

categories = ("neg", "pos")


def extract_rows(filename):
    documents = []
    with open(filename, newline='', mode='r', encoding='UTF8') as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            documents.append((row[0], row[1]))
    return documents


def tokenize(text):
    return [t.lower() for t in re.findall(r'\w+|[^\w\s]+', text) if t.isalpha()]


def train(filename):
    m = {
        'document_count_neg': 0,
        'document_count_pos': 0,
        'word_count_neg': 0,
        'word_count_pos': 0,
        'vocabulary_neg': {},
        'vocabulary_pos': {},
        'vocabulary': {},
    }

    rows = extract_rows(filename)
    for r in rows:
        category = r[0]
        if category == 'neg':
            m['document_count_neg'] += 1
        else:
            m['document_count_pos'] += 1
        text = r[1]
        tokens = tokenize(text)
        for t in tokens:
            m['vocabulary'][t] = m['vocabulary'].get(t, 0) + 1
            if category == 'neg':
                m['word_count_neg'] += 1
                m['vocabulary_neg'][t] = m['vocabulary_neg'].get(t, 0) + 1
            else:
                m['word_count_pos'] += 1
                m['vocabulary_pos'][t] = m['vocabulary_pos'].get(t, 0) + 1

    return m


def test(filename, model):
    p = {
        'category_expected': [],
        'category_predicted': [],
        'neg_correct': 0,
        'neg_incorrect': 0,
        'pos_correct': 0,
        'pos_incorrect': 0,
    }

    vocabulary_size = len(model['vocabulary'])
    probability_prior_neg = math.log(
        model['document_count_neg'] / (model['document_count_neg'] + model['document_count_pos']))
    probability_prior_pos = math.log(
        model['document_count_pos'] / (model['document_count_neg'] + model['document_count_pos']))

    rows = extract_rows(filename)
    for r in rows:
        probability_neg = probability_prior_neg
        probability_pos = probability_prior_pos

        text = r[1]
        tokens = tokenize(text)
        for t in tokens:
            probability_neg += math.log(
                (model['vocabulary_neg'].get(t, 0) + 1) / (model['word_count_neg'] + vocabulary_size))
            probability_pos += math.log(
                (model['vocabulary_pos'].get(t, 0) + 1) / (model['word_count_pos'] + vocabulary_size))

        category_expected = r[0]
        p['category_expected'].append(category_expected)
        if category_expected == 'neg':
            if max(probability_neg, probability_pos) == probability_neg:
                p['neg_correct'] += 1
                p['category_predicted'].append('neg')
            else:
                p['neg_incorrect'] += 1
                p['category_predicted'].append('pos')
        else:
            if max(probability_neg, probability_pos) == probability_pos:
                p['pos_correct'] += 1
                p['category_predicted'].append('pos')
            else:
                p['pos_incorrect'] += 1
                p['category_predicted'].append('neg')

    return p


def evaluate(predictions):
    results = classification_report(predictions['category_expected'], predictions['category_predicted'],
                                    labels=("pos", "neg"), target_names=["Pos", "Neg"])

    print('\n\n###################### MATRICE DE CONFUSION #######################')
    print('\n\n', results, '\n\n')
    print('####################################################################')

    print('Critiques négatives devinées :', predictions['neg_correct'], end=' ')
    print('/', end=' ')
    print(predictions['neg_correct'] + predictions['neg_incorrect'])

    print('Critiques positives devinées :', predictions['pos_correct'], end=' ')
    print('/', end=' ')
    print(predictions['pos_correct'] + predictions['pos_incorrect'])

    correct = predictions['neg_correct'] + predictions['pos_correct']
    incorrect = predictions['neg_incorrect'] + predictions['pos_incorrect']

    accuracy = correct / (correct + incorrect)
    print('Exactitude :', accuracy * 100, "%")


if __name__ == '__main__':
    model = train('movie_review_train.csv')
    predictions = test('movie_review_test.csv', model)
    evaluate(predictions)
